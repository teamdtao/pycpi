# pycpi

A Python library for downloading historical CPI (Consumer Price Index) data.

That is _all_ this library has any ambitions of doing.

## API

This thing has one method. That's all I need.

```python
import pycpi

cpi_data = pycpi.get_data(start_year=1984)

for year, month, value in cpi_data:
    print(f'{year} {month}: {value}')  # e.g. 1984 January: 101.9
```

## Umm...

I searched for about 10 seconds and found two libraries that might have worked instead.

- The README.txt for [python-us-cpi][1] says it doesn't work anymore.
- Time Magazine seems to have a [really great repository][2] with one commit and no code in it.
- I swear there was another one that I'm not seeing now, but it was a very thin wrapper around the
  awful API provided by the BLS (for which I am however grateful).
- Oh, there was also [this one][3] but... that isn't really what I want.

I want what I want!

[1]: https://github.com/Glench/python-us-cpi
[2]: https://github.com/TimeMagazine/inflation-cpi
[3]: https://github.com/datadesk/cpi
